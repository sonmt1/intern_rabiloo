package com.intern.webstoreproducts;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
public class WebstoreproductsApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebstoreproductsApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(WebstoreproductsApplication.class, args);

    }


}
