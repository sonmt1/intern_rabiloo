package com.intern.webstoreproducts.entity.role;

public enum TypePermission {
    HIDDEN, READ_ONLY, EDIT
}
