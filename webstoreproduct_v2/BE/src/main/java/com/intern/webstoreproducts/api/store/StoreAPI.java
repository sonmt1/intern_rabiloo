package com.intern.webstoreproducts.api.store;

import com.intern.webstoreproducts.config.security.CustomUserDetails;
import com.intern.webstoreproducts.dto.ServerResponseDto;
import com.intern.webstoreproducts.dto.store.StoreDto;
import com.intern.webstoreproducts.jwt.JwtTokenProvider;
import com.intern.webstoreproducts.service.StoreService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/web2/store")
@RequiredArgsConstructor
//@CrossOrigin(origins = "http://localhost:8081", allowedHeaders = "*")
public class StoreAPI {

    private static final String AUTHORIZATION_HEADER = "Authorization";
    private final StoreService storeService;
    private final JwtTokenProvider jwtTokenProvider;

    @PostMapping("/list")
//    @PreAuthorize("@authorizationService.hasPermissionAccessViewPage('LIST_STORE')")
    public Page<StoreDto> listWeb(@RequestParam(name = "page", defaultValue = "0") Integer pageStore,
                                  @RequestParam(name = "size", defaultValue = "10") Integer sizeStore,
                                  @RequestParam(defaultValue = "DESC") String sortDir,
                                  @RequestParam(defaultValue = "id") String sortField,
                                  @RequestHeader("Authorization") String token) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            CustomUserDetails userDetails = (CustomUserDetails) authentication.getPrincipal();
        }
        //to do AuthenticationPrincipal
//        Integer storeIdL = customUserDetails.getStoreId();
//        if (storeIdL == null){
//            return Page.empty();
//        }

        System.err.println("token Header: " + token);
        String token1 = token.replaceAll("Bearer\\s*", "");
        System.err.println(token1);
        System.err.println(jwtTokenProvider.validateToken(token1));
        System.err.println(jwtTokenProvider.getUserIdFromJWT(token1));

        return storeService.getAll(pageStore, sizeStore, sortDir, sortField);
    }

    @PreAuthorize("@authorizationService.hasPermissionEditPage('STORE')")
    @PostMapping("/save")
    public ResponseEntity<ServerResponseDto> createStore(@RequestBody StoreDto storeDto,
                                                         @RequestHeader("Authorization") String token) {
        //to do AuthenticationPrincipal
//        Integer storeIdL = customUserDetails.getStoreId();
//        if (storeIdL == null){
//            return ResponseEntity.ok(ServerResponseDto.ERROR);
//        }

        return ResponseEntity.ok(ServerResponseDto.with(storeService.saveStore(storeDto)));
    }

    @DeleteMapping("/delete/{storeId}")
//    @PreAuthorize("@authorizationService.hasPermissionAccessViewPage('LIST_STORE')")
    public ResponseEntity<ServerResponseDto> delete(@PathVariable Integer storeId) {
        //to do AuthenticationPrincipal
//        Integer storeIdL = customUserDetails.getStoreId();
//        if (storeIdL == null) {
//            return ResponseEntity.ok(ServerResponseDto.ERROR);
//        }
        return ResponseEntity.ok(storeService.deleteStore(storeId));
    }

    @GetMapping(value = "/search")
    public Page<StoreDto> searchStore(@RequestParam String value) {
        return storeService.search(value, 0, 10, "ACS");
    }

}
