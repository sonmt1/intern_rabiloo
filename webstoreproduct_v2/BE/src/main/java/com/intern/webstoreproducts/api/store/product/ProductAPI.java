package com.intern.webstoreproducts.api.store.product;

import com.intern.webstoreproducts.dto.ResponseCase;
import com.intern.webstoreproducts.dto.ServerResponseDto;
import com.intern.webstoreproducts.dto.store.product.ProductDto;
import com.intern.webstoreproducts.entity.store.product.Product;
import com.intern.webstoreproducts.service.ProductService;
import com.intern.webstoreproducts.service.StoreService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/v1/web2/{storeCode}/product")
@RequiredArgsConstructor
public class ProductAPI {
    private final ProductService productService;
    private final StoreService storeService;

    @GetMapping("/list")
    public Page<ProductDto> listWeb(@PathVariable String storeCode,
                                    @RequestParam(name = "page", defaultValue = "0") Integer pageProduct,
                                    @RequestParam(name = "size", defaultValue = "10") Integer sizeProduct,
                                    @RequestParam(defaultValue = "DESC") String sortDir,
                                    @RequestParam(defaultValue = "id") String sortField) {
        //to do authorizationService.hasPermissionAccessStore(#storeCode)
        //to do AuthenticationPrincipal
        Integer storeId = storeService.getStoreIdByStoreCode(storeCode);
        if (storeId == null) {
            return Page.empty();
        }
        return productService.getAllByStoreId(storeId, pageProduct, sizeProduct, sortDir, sortField);
    }

    @PostMapping("/save")
    public ResponseEntity<ServerResponseDto> createProduct(@PathVariable String storeCode,
                                                           @RequestBody Product product) {
        //to do authorizationService.hasPermissionAccessStore(#storeCode)
        //to do AuthenticationPrincipal
        Integer storeId = storeService.getStoreIdByStoreCode(storeCode);
        ResponseCase result = productService.saveProduct(storeId, product);
        return ResponseEntity.ok(ServerResponseDto.with(result));
    }

    @DeleteMapping("/delete/{productId}")
    public ResponseEntity<ServerResponseDto> delete(@PathVariable String storeCode, @PathVariable Integer productId) {
        //to do authorizationService.hasPermissionAccessStore(#storeCode)
        //to do AuthenticationPrincipal
        Integer storeId = storeService.getStoreIdByStoreCode(storeCode);
        return ResponseEntity.ok(productService.deleteProduct(storeId, productId));
    }
}
