package com.intern.webstoreproducts.repository.store.product;

import com.intern.webstoreproducts.dto.store.product.ProductDto;
import com.intern.webstoreproducts.entity.store.product.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {



    @Query("select p.id as id, p.code as code, p.name as name, p.detail as detail " +
            "from Product p where p.storeId = ?1 and p.isDeleted = false ")
    Page<ProductDto> findAllByStoreIdAndIsDeletedFalse(Integer storeId, Pageable pageable);

    @Query("select p from Product p where p.storeId = ?1 and p.code = ?2")
    Product findByStoreIdAndCode(Integer storeId, String code);

    @Query("select p from Product p where p.storeId = ?1 and p.id = ?2 and p.isDeleted = ?3")
    Product findByStoreIdAndIdAndIsDeleted(Integer storeId, Integer productId, Boolean isDeleted);

    @Query("select p.code from Product p where p.id = (select max(p1.id) from Product p1)")
    String findLastCode();

    Product findByIdAndIsDeleted(Integer id, Boolean isDeleted);

    @Query("select p from Product p where p.storeId = ?1 and p.name = ?2 and p.isDeleted = ?3")
    Product findByStoreIdAndNameAndIsDeleted(Integer storeId, String name, boolean isDeleted);

    @Query("select p from Product p where p.code = ?1")
    Optional<Product> findByCode(String newCode);
}
