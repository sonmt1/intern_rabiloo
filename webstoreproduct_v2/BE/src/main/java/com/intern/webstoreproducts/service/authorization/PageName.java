package com.intern.webstoreproducts.service.authorization;

public enum PageName {
    STORE,
    PRODUCT,
    HOME
}
