package com.intern.webstoreproducts.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.intern.webstoreproducts.util.Constant;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ResponseCase {
    SUCCESS(1000, Constant.SUCCESS_MSG),
    ADD_SUCCESS(2000, Constant.SUCCESS_MSG),
    UPDATE_SUCCESS(2001, Constant.SUCCESS_MSG),
    DELETE_SUCCESS(2002, Constant.SUCCESS_MSG),
    DELETE_FAILED(2005, "ERROR"),
    ERROR(4, "Error"),
    DUPLICATE(1020, "Duplicate"),
    NOT_FOUND(1030, "Cannot found entity!"),
    IN_USE(1031, "In use!"),
    WRONG_USERNAME(1032, "Wrong username!"),
    WRONG_PASSWORD(1033, "Wrong password!"),
    DUPLICATE_DATA(1043, "Duplicate data!"),
    BAD_REQUEST(400, "Invalid Parameters!"),
    INPUT_EMPTY(1040,"Input is empty!");


    public final int code;
    public final String message;

    ResponseCase(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
