package com.intern.webstoreproducts.dto.login;

import com.intern.webstoreproducts.dto.ResponseCase;
import lombok.Getter;

@Getter

public class LoginResponseDto {

    private final ResponseCase status;
    private final String tokenType="Bearer ";
    private final String accessToken;

    public LoginResponseDto(String accessToken, ResponseCase status) {
        this.accessToken = accessToken;
        this.status = status;
    }

    public static LoginResponseDto notFound() {
        return new LoginResponseDto(null, ResponseCase.NOT_FOUND);
    }

    public static LoginResponseDto success(String accessToken) {
        return new LoginResponseDto(accessToken, ResponseCase.SUCCESS);
    }

    public static LoginResponseDto wrongUsername() {
        return new LoginResponseDto(null, ResponseCase.WRONG_USERNAME);
    }

    public static LoginResponseDto wrongPassword() {
        return new LoginResponseDto(null, ResponseCase.WRONG_PASSWORD);
    }


}
