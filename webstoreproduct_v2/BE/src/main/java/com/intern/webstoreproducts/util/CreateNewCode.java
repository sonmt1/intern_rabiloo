package com.intern.webstoreproducts.util;

import com.intern.webstoreproducts.repository.store.product.ProductRepository;
import com.intern.webstoreproducts.repository.store.StoreRepository;
import lombok.experimental.UtilityClass;
import org.springframework.data.jpa.repository.JpaRepository;

import java.text.DecimalFormat;

@UtilityClass
public class CreateNewCode {
    //Ham xu ly chuoi de tao code product
    public static String createCode(JpaRepository repository) {


        String lastCode;
        String newCode = "";
        if (repository instanceof ProductRepository) {
            //lay code cuoi cung cua product: "PXXXXX"
            lastCode = ((ProductRepository) repository).findLastCode();
            if (lastCode == null) {
                return "P00001";
            }
            newCode = generateCode(lastCode);
            while (true) {
                if ((((ProductRepository) repository).findByCode(newCode)).isPresent()) {
                    newCode = generateCode(newCode);
                    continue;
                }
                break;
            }
        } else if (repository instanceof StoreRepository) {
            //lay code cuoi cung cua store: "SPXXXXX"
            lastCode = ((StoreRepository) repository).findLastCode();
            System.err.println(lastCode);
            if (lastCode == null) {
                return "SP00001";
            }
            newCode = generateCode(lastCode);
            while (true) {
                if ((((StoreRepository) repository).findByCode(newCode)).isPresent()) {
                    newCode = generateCode(newCode);
                    continue;
                }
                break;
            }
        }


        return newCode;
    }

    public static String generateCode(String lastCode) {
        //lay ky tu dau bieu dien code
        String newCode = lastCode.replaceAll("[0-9]", "");
        //thay the toan bo ky tu khong phai so ->"SP" -> ""
        lastCode = lastCode.replaceAll("[^0-9]", "");
        //Lay du lieu so trong code de xu ly tao code moi
        //to do xu ly mat cac so 0 dang truoc do
        Integer codeNumber = Integer.parseInt(lastCode);
        codeNumber++;
        //format dinh dang string
        DecimalFormat df = new DecimalFormat("#00000");
        //convert codeNumber -> string theo ding dang df
        String codeNumberString = df.format(codeNumber);
        //Them codeNumberString vao newCode va tra ve du lieu moi
        newCode = newCode.concat(codeNumberString);
        System.err.println(newCode);
        return newCode;
    }
}
