package com.intern.webstoreproducts.api.login;

import com.intern.webstoreproducts.config.security.WebSecurityConfig;
import com.intern.webstoreproducts.dto.login.LoginRequestDto;
import com.intern.webstoreproducts.dto.login.LoginResponseDto;
import com.intern.webstoreproducts.entity.user.User;
import com.intern.webstoreproducts.jwt.JwtTokenProvider;
import com.intern.webstoreproducts.repository.user.UserRepository;
import com.intern.webstoreproducts.service.user.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("")
@RequiredArgsConstructor
@Slf4j
public class LoginApi {

    private final WebSecurityConfig webSecurityConfig;

    private final PasswordEncoder passwordEncoder;

    private final UserRepository userRepository;

    private final UserService userService;

    @Autowired
    private JwtTokenProvider tokenProvider;


    /*
    login
    ?? UsernamePasswordAuthenticationToken: kiem tra username va password hop le
    ?? authenticationManager.authenticate: kiem tra nguoi quan ly
    kiem tra hop le -> tao authentication voi username va password duoc gui tu request
    luu authentication -> SecurityContextHolder.getContext()
    -> kiem tra cac api sau khi login
     */
    @PostMapping("/api/login")
    public ResponseEntity<LoginResponseDto> login(@RequestBody LoginRequestDto loginRequestDto) {
        System.err.println("loginApi");
        //todo: return an object with jwt token


//        log.info("LoginRequestDto: "+loginRequestDto.getUsername()+":"+loginRequestDto.getPassword());
//        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
//                loginRequestDto.getUsername(),
//                loginRequestDto.getPassword()
//        );
//
//        log.info("authenticationToken: "+authenticationToken.getCredentials().toString());
//        //Xac thuc thong tin nguoi dung Request len
//        Authentication authentication = authenticationManager.authenticate(authenticationToken);
        User userDetails = userService.getByUsername(loginRequestDto.getUsername());
        if (userDetails == null) {
            return ResponseEntity.ok(LoginResponseDto.wrongUsername());
        }
        if (!passwordEncoder.matches(loginRequestDto.getPassword(), userDetails.getPassword())) {
            return ResponseEntity.ok(LoginResponseDto.wrongPassword());
        }
        Authentication authentication = new UsernamePasswordAuthenticationToken(userDetails.getUsername(), userDetails.getPassword());

//        Neu khong xay ra exception tuc la thong tin hop le
//        Set thong tin authentication vao Security Context
        SecurityContextHolder.getContext().setAuthentication(authentication);


        String jwt = tokenProvider.generateTokenByUser(userDetails);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Set-Cookie", String.format("auth._token.local=%s;Max-Age=86400;Path=/;", "Baerer%20" + jwt));

        return ResponseEntity.status(HttpStatus.OK).headers(headers).body(LoginResponseDto.success(jwt));
    }

    @PostMapping("/save")
    public String save(@RequestBody LoginRequestDto loginRequestDto) {
        userRepository.save(new User(loginRequestDto.getUsername(), passwordEncoder.encode(loginRequestDto.getPassword()), "ROLE_STAFF"));
        return "ok";

    }

    @GetMapping("/login?logout")
    public String logout() {
        return "login";
    }

}
