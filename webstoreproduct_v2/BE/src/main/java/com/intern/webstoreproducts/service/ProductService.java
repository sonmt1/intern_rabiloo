package com.intern.webstoreproducts.service;

import com.intern.webstoreproducts.dto.ResponseCase;
import com.intern.webstoreproducts.dto.ServerResponseDto;
import com.intern.webstoreproducts.dto.store.product.ProductDto;
import com.intern.webstoreproducts.entity.store.product.Product;
import com.intern.webstoreproducts.repository.store.product.ProductRepository;
import com.intern.webstoreproducts.util.CreateNewCode;
import com.intern.webstoreproducts.util.PageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Objects;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;


    public Page<ProductDto> getAllByStoreId(Integer storeId, Integer pageProduct, Integer sizeProduct, String sortDir, String sortField) {

        return productRepository.findAllByStoreIdAndIsDeletedFalse(storeId,  PageUtil.getPage(pageProduct, sizeProduct, sortDir, sortField));
    }

    @Transactional
    public ResponseCase saveProduct(Integer storeId, Product product) {
        if(product.getName().equals("") || product.getDetail().equals("")){
            return ResponseCase.INPUT_EMPTY;
        }
        Product entityWithSameName = productRepository.findByStoreIdAndNameAndIsDeleted(storeId, product.getName(), false);

        if (entityWithSameName != null && !Objects.equals(product.getId(), entityWithSameName.getId())) {
            return ResponseCase.DUPLICATE;
        }

        try {
            Product oldEntity = productRepository.findByStoreIdAndIdAndIsDeleted(storeId, product.getId(), false);
            product.setStoreId(storeId);
            //update product
            if (oldEntity != null) {
                oldEntity.setName(product.getName());
                oldEntity.setDetail(product.getDetail());
                productRepository.save(oldEntity);
            }
            //create product
            else {
                //create code
                product.setCode(CreateNewCode.createCode(productRepository));
                product.setIsDeleted(false);
                productRepository.save(product);
            }
        } catch (DataIntegrityViolationException e) {
            return ResponseCase.DUPLICATE;
        }
        return ResponseCase.SUCCESS;
    }

    public ServerResponseDto deleteProduct(Integer storeId, Integer productId) {
        Product product = productRepository.findByStoreIdAndIdAndIsDeleted(storeId, productId, false);
        if (product == null) {
            return ServerResponseDto.with(ResponseCase.NOT_FOUND);
        }
        product.setIsDeleted(true);
        productRepository.save(product);
        return ServerResponseDto.SUCCESS;
    }
}
