package com.intern.webstoreproducts.service.user;

import com.intern.webstoreproducts.config.security.CustomUserDetails;
import com.intern.webstoreproducts.dto.login.LoginRequestDto;
import com.intern.webstoreproducts.dto.login.LoginResponseDto;
import com.intern.webstoreproducts.entity.user.User;
import com.intern.webstoreproducts.repository.user.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@AllArgsConstructor
@Slf4j
public class UserService implements UserDetailsService {


    private final UserRepository userRepository;


//    public LoginResponseDto login(LoginRequestDto loginRequestDto) {
//        /* B1: tìm user trong db với username, nếu ko có return object lỗi
//         * B2: check user password hash với password param. nếu ko match password return object lỗi
//         * B3: generate token từ user đó
//         * B4: return object success
//         */
//
//        Authentication authentication = authenticationManager.authenticate(
//                new UsernamePasswordAuthenticationToken(
//                        loginRequestDto.getUsername(),
//                        loginRequestDto.getPassword()
//                )
//        );
//
//        SecurityContextHolder.getContext().setAuthentication(authentication);
//
//        String jwt = jwtTokenProvider.generateTokenByUsername(((CustomUserDetails) authentication.getPrincipal()).getUsername());
//
////        System.err.println(loginRequestDto.getUsername() + ":" + loginRequestDto.getPassword());
////        Optional<User> userOptional = userRepository.findByUsername(loginRequestDto.getUsername());
////
////        if (userOptional.isEmpty()) {
////            return LoginResponseDto.notFound();
////        }
////        User user = userOptional.get();
////
////
////        if (!user.getPassword().equals(passwordEncoder.encode(loginRequestDto.getPassword()))) {
////            return LoginResponseDto.notFound();
////        }
////        String jwt = jwtTokenProvider.generateTokenById(user.getId());
//        LOGGER.info("JWT: "+jwt);
//        return LoginResponseDto.success(jwt);
//    }



    @Override
    public CustomUserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        log.info("username"+username);
        Optional<User> user = userRepository.findByUsername(username);
        if (user.isEmpty()) {
            throw new UsernameNotFoundException(username);
        }
        log.info(user.get().getPassword());
        return new CustomUserDetails(user.get());
    }

    public User getByUsername(String username) throws UsernameNotFoundException {
        log.info("username"+username);
        Optional<User> user = userRepository.findByUsername(username);
        if (user.isEmpty()) {
            throw new UsernameNotFoundException(username);
        }
        log.info(user.get().getPassword());
        return user.get();
    }

    // JWTAuthenticationFilter sẽ sử dụng hàm này
    @Transactional
    public CustomUserDetails loadUserById(Long id) {
        Optional<User> user = userRepository.findById(id);
        if (user.isEmpty()) {
            throw new UsernameNotFoundException("User not found with id: " + id);
        }
        return new CustomUserDetails(user.get().getId());
    }

    public LoginResponseDto login(LoginRequestDto loginRequestDto) {


        LoginResponseDto loginResponseDto = null;
        return loginResponseDto;
    }


//    // lay UserDetails bang user voi username
//    @Override
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//
//        Optional<User> user = userRepository.findByUsername(username);
//        if (user.isEmpty()){
//            throw new UsernameNotFoundException(username);
//        }
//        return new CustomUserDetails(user.get());
//    }
}
