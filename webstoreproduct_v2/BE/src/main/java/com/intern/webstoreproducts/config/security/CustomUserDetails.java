package com.intern.webstoreproducts.config.security;

import com.intern.webstoreproducts.config.security.enums.Role;
import com.intern.webstoreproducts.dto.role.RoleDto;
import com.intern.webstoreproducts.entity.user.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.minidev.json.annotate.JsonIgnore;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomUserDetails implements UserDetails {
    @JsonIgnore
    private String accessToken;
    private Long userId;
    private String username;
    private String password;
    private Collection<String> listRole;

    public CustomUserDetails(String username) {
        this.username = username;
    }

    public CustomUserDetails(Long userId) {
        this.userId = userId;
    }

    public CustomUserDetails(User user) {
        this.userId = user.getId();
        this.username  = user.getUsername();
        this.password = user.getPassword();
    }

    public boolean hasAnyRole(Role... roles){
        for (Role r :
                roles) {
            if (isRole(r)){
                return true;
            }
        }
        return false;
    }

    private boolean isRole(Role role) {
        return this.listRole.contains(role.toString());
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        //todo: update this;
        List<GrantedAuthority> authorities = new ArrayList<>();
        this.listRole
                .forEach(role -> authorities.add(new SimpleGrantedAuthority(role)));
        return authorities;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        return false;
    }
}
