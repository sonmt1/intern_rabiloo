package com.intern.webstoreproducts.config.security.enums;

public enum Role {
    ROLE_ADMIN,
    ROLE_STAFF,
    ROLE_USER,
}
