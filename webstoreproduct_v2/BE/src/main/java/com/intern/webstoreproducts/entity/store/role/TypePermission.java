package com.intern.webstoreproducts.entity.store.role;

public enum TypePermission {
    HIDDEN, READ_ONLY, EDIT, CREATE, DELETE
}
