package com.intern.webstoreproducts.dto.store.product;



public interface ProductDto {
    Integer getId();
    String getCode();
    String getName();
    String getDetail();
}
