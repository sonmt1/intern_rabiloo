package com.intern.webstoreproducts.entity.store.role;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@Entity
@Table(name = "role")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder(toBuilder = true)
public class RoleEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Integer storeId;
    private String name;
    private String code;
    private String remark;

    @Enumerated(EnumType.STRING)
    private TypePermission storePermission;
    @Enumerated(EnumType.STRING)
    private TypePermission productPermission;

    private Boolean isDeleted;


}
