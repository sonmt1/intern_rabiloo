package com.intern.webstoreproducts.dto.role;

import com.intern.webstoreproducts.entity.store.role.RoleEntity;
import com.intern.webstoreproducts.entity.store.role.TypePermission;
import lombok.*;

@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class RoleDto {
    private Integer id;
    private String name;
    private String remark;

    private TypePermission storePermission;
    private TypePermission productPermission;

    public static RoleDto fromEntity(RoleEntity roleEntity){
        return RoleDto.builder()
                .productPermission(roleEntity.getProductPermission())
                .storePermission(roleEntity.getStorePermission())
                .build();
    }
}
