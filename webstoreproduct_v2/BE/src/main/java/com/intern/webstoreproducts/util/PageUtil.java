package com.intern.webstoreproducts.util;

import lombok.experimental.UtilityClass;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

@UtilityClass
public class PageUtil {
    public static Pageable getPage(int page, int size, String sortDir, String sortField) {
        if (page <= 0) page = 0;
        if (size <= 0) size = 1;
        if (size > 1000) size = 1000;

        if (Constant.ASC.equalsIgnoreCase(sortDir) || sortDir.equalsIgnoreCase("ascending")) {
            return PageRequest.of(page, size, Sort.Direction.ASC, sortField);
        }

        return PageRequest.of(page, size, Sort.Direction.DESC, sortField);
    }
}
