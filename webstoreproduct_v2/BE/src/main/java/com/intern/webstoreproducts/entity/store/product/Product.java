package com.intern.webstoreproducts.entity.store.product;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "product")
@Getter
@Setter
@NoArgsConstructor
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String code;
    private Integer storeId;
    private String name;
    private String detail;
    private Boolean isDeleted;

    public Product(Integer id, String code, Integer storeId, String name, String detail, Boolean isDeleted) {
        this.id = id;
        this.code = code;
        this.storeId = storeId;
        this.name = name;
        this.detail = detail;
        this.isDeleted = isDeleted;
    }
}
