package com.intern.webstoreproducts.jwt;

import com.intern.webstoreproducts.config.security.CustomUserDetails;
import com.intern.webstoreproducts.entity.user.User;
import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
@Slf4j
public class JwtTokenProvider {
    public static final Logger LOGGER = LoggerFactory.getLogger(JwtTokenProvider.class);

    // doan ma hoa bi mat, chi server biet
    private final String JWT_SECRET = "123ZO";

    // Tao jwt tu thong tin user
    public String generateTokenById(long id) {
        Date now = new Date();
        //thoi gian hieu luc cua chuoi jwt
        long JWT_EXPIRATION = 999999L;
        Date expriyDate = new Date(now.getTime() + JWT_EXPIRATION);

        //Tao chuoi json web token tu id cua User
        //todo: hash user role in token
        return Jwts.builder()
                .setSubject(Long.toString(id))
                .setIssuedAt(now)
                .setExpiration(expriyDate)
                .signWith(SignatureAlgorithm.HS512, JWT_SECRET)
                .compact();
    }

    // Tao jwt tu thong tin user
    public String generateTokenByUser(User user) {
        Date now = new Date();
        //thoi gian hieu luc cua chuoi jwt
        long JWT_EXPIRATION = 999999L;
        Date expriyDate = new Date(now.getTime() + JWT_EXPIRATION);

        //Tao chuoi json web token tu id cua User
        //todo: hash user role in token
        Map<String, Object> claims = new HashMap<>();
        claims.put("role", user.getRole());
        claims.put("name", user.getUsername());

        return Jwts.builder()
                .setSubject(Long.toString(user.getId()))
                .addClaims(claims)
                .setIssuedAt(now)
                .setExpiration(expriyDate)
                .signWith(SignatureAlgorithm.HS512, JWT_SECRET)
                .compact();
    }


    //lay thong tin user tu jwt
    // todo: CustomUserDetails
    public Optional<CustomUserDetails> getUserIdFromJWT(String token) {
        try {
            Claims claims = Jwts.parser()
                    .setSigningKey(JWT_SECRET)
                    .parseClaimsJws(token)
                    .getBody();

            CustomUserDetails userDetails = new CustomUserDetails();
            userDetails.setListRole(Set.of(claims.get("role").toString()));
            userDetails.setUserId(Long.valueOf(claims.getSubject()));
            userDetails.setUsername((String) claims.get("name"));
            return Optional.of(userDetails);
        } catch (Exception e) {
            log.error("Exception when cast user details from token!", e);
            return Optional.empty();
        }
    }

    public boolean validateToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(JWT_SECRET).parseClaimsJws(authToken);
            return true;
        } catch (MalformedJwtException | ExpiredJwtException | UnsupportedJwtException | IllegalArgumentException ex) {
            LOGGER.error(ex.toString());
        }
        return false;
    }
}
