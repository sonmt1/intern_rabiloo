package com.intern.webstoreproducts.config.security;

import lombok.experimental.UtilityClass;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

@UtilityClass
public class AuthenticationUtils {
    public static boolean isAuthenticated() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return false;
        }
        return authentication.isAuthenticated();
    }

    public static CustomUserDetails getUserDetails() {
        return isAuthenticated() ? (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal() : null;

    }

}
