package com.intern.webstoreproducts.service;

import com.intern.webstoreproducts.dto.ResponseCase;
import com.intern.webstoreproducts.dto.ServerResponseDto;
import com.intern.webstoreproducts.dto.store.StoreDto;
import com.intern.webstoreproducts.entity.store.Store;
import com.intern.webstoreproducts.exception.StoreNotFoundException;
import com.intern.webstoreproducts.repository.store.StoreRepository;
import com.intern.webstoreproducts.util.CreateNewCode;
import com.intern.webstoreproducts.util.PageUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class StoreService {

    private final StoreRepository storeRepository;

    public Integer getStoreIdByStoreCode(String storeCode) {
        System.err.println(storeCode + " 1");
        Optional<StoreDto> storeDto = getStoreByStoreCode(storeCode);
        return storeDto.map(StoreDto::getId).orElseThrow(() -> new StoreNotFoundException("Store not found with code: " + storeCode));
    }

    public Optional<StoreDto> getStoreByStoreCode(String storeCode) {
        Optional<Store> storeOptional = storeRepository.findByCodeAndIsDeletedFalse(storeCode);
        Store store = storeOptional.get();
        StoreDto storeDto = new StoreDto(store.getId(),store.getCode(), store.getName(), store.getAddress());
        if (!storeOptional.isEmpty()) {
            return Optional.of(storeDto);
        }
        System.err.println("null");
        return Optional.empty();

    }

    public Page<StoreDto> getAll(Integer page, Integer size, String sortDir, String sortField) {
        return storeRepository.getAllAndIsDeletedFalse(PageUtil.getPage(page, size, sortDir, sortField));
    }

    public ResponseCase saveStore(StoreDto storeDto) {
        if(storeDto.getName().equals("") || storeDto.getAddress().equals("")){
            return ResponseCase.INPUT_EMPTY;
        }
        Store entityWithSameName = storeRepository.findByNameAndIsDeleted(storeDto.getName(), false);
        if (entityWithSameName != null && !Objects.equals(storeDto.getId(), entityWithSameName.getId())) {
            return ResponseCase.DUPLICATE;
        }

        try {
            //to do save by user
            Store oldEntity = storeRepository.findByIdAndIsDeleted(storeDto.getId(), false);
            if (oldEntity != null) {
                oldEntity.setName(storeDto.getName());
                oldEntity.setAddress(storeDto.getAddress());
                storeRepository.save(oldEntity);
            } else {
                Store newEntity = StoreDto.toEntity(storeDto);
                newEntity.setCode(CreateNewCode.createCode(storeRepository));
                newEntity.setIsDeleted(false);
                storeRepository.save(newEntity);
            }
        } catch (DataIntegrityViolationException e) {
            return ResponseCase.DUPLICATE;
        }
        return ResponseCase.SUCCESS;
    }

    public ServerResponseDto deleteStore(Integer storeId) {
        Store store = storeRepository.findByIdAndIsDeleted(storeId, false);
        if (store == null) {
            return ServerResponseDto.with(ResponseCase.NOT_FOUND);
        }
        store.setIsDeleted(true);
        storeRepository.save(store);
        return ServerResponseDto.SUCCESS;
    }

    public Page<StoreDto> search(String value, int page, int size, String sortDir) {
        return storeRepository.findByCodeLikeOrNameLikeOrAddressLike(PageUtil.getPage(page,size,sortDir,"name"), value);
    }
}
