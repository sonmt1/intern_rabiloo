package com.intern.webstoreproducts.config.filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.cors.CorsUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
@Slf4j
public class CORSFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        //do nothing
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse responseHttpServlet = (HttpServletResponse) response;
        HttpServletRequest requestHttpServlet = (HttpServletRequest) request;
        String origin = requestHttpServlet.getHeader("Origin");

        responseHttpServlet.setHeader("Access-Control-Allow-Origin", origin);
        responseHttpServlet.setHeader("Access-Control-Allow-Credentials", "true");
        responseHttpServlet.setHeader("Access-Control-Allow-Methods", "DELETE, GET, HEAD, OPTIONS, POST, PUT, REPORT, SEARCH, UPDATE, VERSION-CONTROL");
        responseHttpServlet.setHeader("Access-Control-Max-Age", "3600");
        responseHttpServlet.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, " +
                "Key, Authorization, Company-Code, App-Key, X-Forwarded-CompanyId, X-Forwarded-Email, " +
                "X-Forwarded-Original-UserId, X-Forwarded-UserId, X-Forwarded-UserName, X-Forwarded-Roles, " +
                "X-Forwarded-Services, X-Forwarded-CompanyCode, X-Forwarded-CompanyName" + "Authorization");
        if (CorsUtils.isPreFlightRequest(requestHttpServlet)) {
            return;
        }
        if ("OPTIONS".equalsIgnoreCase(requestHttpServlet.getMethod())) {
            responseHttpServlet.setStatus(HttpServletResponse.SC_OK);
        } else {
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {
        // do nothing
    }
}
