package com.intern.webstoreproducts.repository.store.role;

import com.intern.webstoreproducts.entity.store.role.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.List;

public interface RoleRepository extends JpaRepository<RoleEntity, Integer> {


    @Query("select r from RoleEntity r where r.code in ?1 and r.isDeleted = false")
    List<RoleEntity> findByCodeInAndAndIsDeletedFalse(Collection<String> codes);
}
