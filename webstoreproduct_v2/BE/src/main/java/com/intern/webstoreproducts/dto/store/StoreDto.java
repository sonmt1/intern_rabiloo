package com.intern.webstoreproducts.dto.store;

import com.intern.webstoreproducts.entity.store.Store;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class StoreDto {
    private Integer id;
    private String code;
    private String name;
    private String address;

    public static Store toEntity(StoreDto storeDto) {
        return new Store(storeDto.getId(),storeDto.getCode(),storeDto.getName(), storeDto.getAddress());
    }
}
