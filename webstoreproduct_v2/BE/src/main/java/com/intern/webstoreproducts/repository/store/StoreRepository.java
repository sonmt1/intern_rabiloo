package com.intern.webstoreproducts.repository.store;

import com.intern.webstoreproducts.dto.store.StoreDto;
import com.intern.webstoreproducts.entity.store.Store;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StoreRepository extends JpaRepository<Store, Integer> {

    @Query("select s from Store s where s.code = ?1")
    Optional<Store> findByCodeAndIsDeletedFalse(String storeCode);

    @Query("select s.code from Store s where s.id = (select max(s1.id) from Store s1)")
    String findLastCode();

    //    order by s.id desc
    @Query("select new com.intern.webstoreproducts.dto.store.StoreDto(s.id, s.code, s.name, s.address)" +
            " from Store s where s.isDeleted = false")
    Page<StoreDto> getAllAndIsDeletedFalse(Pageable pageable);

    @Query("select s from Store s where s.name = ?1 and s.isDeleted = ?2")
    Store findByNameAndIsDeleted(String name, Boolean isDeleted);

    Store findByIdAndIsDeleted(Integer id, Boolean isDeleted);

    Optional<Store> findByCode(String newCode);

    @Query("select new com.intern.webstoreproducts.dto.store.StoreDto(s.id, s.code, s.name, s.address)" +
            " from Store s where s.code like concat('%',?1,'%') or s.name like concat('%',?1,'%') or s.address like concat('%',?1,'%')")
    Page<StoreDto> findByCodeLikeOrNameLikeOrAddressLike(Pageable pageable, String name);
}
