package com.intern.webstoreproducts.repository.user;

import com.intern.webstoreproducts.dto.user.UserDto;
import com.intern.webstoreproducts.entity.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    @Query("select new com.intern.webstoreproducts.dto.user.UserDto(u.username,u.password) from User u where u.username = ?1")
    Optional<UserDto> findDtoByUsername(String username);

    @Query("select u from User u where u.id = ?1")
    Optional<User> findById(Long id);

    @Query("select u from User u where u.username = ?1")
    Optional<User>findByUsername(String username);
}
