package com.intern.webstoreproducts.service.authorization;

import com.intern.webstoreproducts.config.security.AuthenticationUtils;
import com.intern.webstoreproducts.config.security.CustomUserDetails;
import com.intern.webstoreproducts.config.security.enums.Role;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service(value = "authorizationService")
@RequiredArgsConstructor
@Slf4j
public class PreAuthorizeService {

    public boolean hasPermissionEditPage(PageName pageName) {
        System.err.println("VAO DAY");
        CustomUserDetails userDetails = AuthenticationUtils.getUserDetails();
        if (userDetails == null) {
            return false;
        }

        log.info(userDetails.toString());
        return userDetails.hasAnyRole(Role.ROLE_ADMIN) && pageName.equals(PageName.STORE);
    }

}
