package com.intern.webstoreproducts.jwt;

import com.intern.webstoreproducts.config.security.CustomUserDetails;
import com.intern.webstoreproducts.service.user.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Component
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    public static final List<String> IGNORE_CHECK_TOKEN_PATHS = List.of("/login", "/api/save");

    private final JwtTokenProvider tokenProvider;
    private final UserService customUserDetailsService;


    /*
    kiem tra jwt sau khi da dang nhap, gui request -> lay du lieu
    kiem tra co trung voi api request k can kiem tra token
    lay jwt tu request
    kiem tra jwt k null va dung dang token
    la id user tu jwt
    set userDetails -> tim userdetails tu id qua cutomUserDetailsService
    tao authentication voi userDetails, va list quyen cua user
    ?? authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
    ?? SecurityContextHolder.getContext().setAuthentication(authentication); lay context cua authentication
    */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            System.err.println("request.getRequestURI(): " + request.getRequestURI());
            if (IGNORE_CHECK_TOKEN_PATHS.stream().anyMatch(request.getRequestURI()::contains)) {
                filterChain.doFilter(request, response);
                return;
            }
            String jwt = getJwtFromRequest(request);
            log.info("jwt: " + jwt);

            if (StringUtils.hasText(jwt) && tokenProvider.validateToken(jwt)) {
                //kiem tra jwt k null va dung dang
                Optional<CustomUserDetails> userDetails = tokenProvider.getUserIdFromJWT(jwt);
                log.info("userDetails: " + userDetails);
                if (userDetails.isPresent()) {
                    UsernamePasswordAuthenticationToken authentication =
                            new UsernamePasswordAuthenticationToken(userDetails.get(), null, userDetails.get().getAuthorities());

                    SecurityContextHolder.getContext().setAuthentication(authentication);
                    log.info("authentication: " + authentication);
                } else {
                    response.setStatus(401);
                    return;
                }
            } else {
                response.setStatus(401);
                return;
            }
        } catch (Exception ex) {
            log.error("failed on set user authentication", ex);
        }
        filterChain.doFilter(request, response);
    }

    private String getJwtFromRequest(HttpServletRequest request) {
        log.info("request: " + request);
        String bearerToken = request.getHeader("Authorization");
        log.info("bearerToken: " + bearerToken);
        //Kiem tra xem header Authorization co chua thong tin jwt khong
        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7);
        }
        return null;
    }


}
