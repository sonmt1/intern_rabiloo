package com.intern.webstoreproducts.dto.role;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class RoleForCommonDto {
    private static final String ROLE_PREFIX = "STORE_";

    private String name;

    @JsonProperty(value = "code_role")
    private String code;

    public RoleForCommonDto(String name, String code) {
        this.name = name;
        this.code = code;
    }

    public static String convertToOriginalCode(String code){
        return code.replace(ROLE_PREFIX,"");
    }
}
