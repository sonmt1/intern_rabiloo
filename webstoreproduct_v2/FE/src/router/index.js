import Vue from "vue";
import VueRouter from "vue-router";
import ViewStore from "@/components/ViewStore";
import ViewProductInStore from "@/components/ViewProductInStore";
import PageNotFound from "@/components/public.error/PageNotFound"
import HomePage from "@/components/HomePage";
import LoginForm from "@/components/login/LoginForm"
import LogoutForm from "@/components/login/LogoutForm"

Vue.use(VueRouter);


export const router = new VueRouter({
    mode: "history",
    routes: [
        {path: '/', component: HomePage, name: 'home'},
        {path: '/ViewStore', component: ViewStore, name: 'store'},
        {path: '/ViewProductInStore/:store*', component: ViewProductInStore, name: 'product'},
        {path: '/Login', component: LoginForm, name: 'login'},
        {path: '/Logout', component: LogoutForm, name: 'logout'},
        {path: '/:pathMatch(.*)*', component: PageNotFound, name: '404'}
    ]
});

