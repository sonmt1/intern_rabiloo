package com.example.demoLiquiBase1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoLiquiBase1Application {

	public static void main(String[] args) {
		SpringApplication.run(DemoLiquiBase1Application.class, args);
	}

}
