package com.intern.webstoreproducts.old.repository;

import com.intern.webstoreproducts.old.model.StoreProducts;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StoreProductsRepository extends JpaRepository<StoreProducts,Integer> {
//    List<StoreProducts> findByIdStore(int idStore);

    StoreProducts findByIdStoreAndIdProduct(int idStore, int idProduct);

    Page<StoreProducts> findByIdStore(Integer idStore, Pageable pageable);
}
