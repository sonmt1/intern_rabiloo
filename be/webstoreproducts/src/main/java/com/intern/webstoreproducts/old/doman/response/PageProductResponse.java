package com.intern.webstoreproducts.old.doman.response;

import com.intern.webstoreproducts.old.comon.BaseResponse;
import com.intern.webstoreproducts.old.model.Product;
import org.springframework.data.domain.Page;

public class PageProductResponse extends BaseResponse {
    Page<Product> productPage;

    public Page<Product> getProductPage() {
        return productPage;
    }

    public void setProductPage(Page<Product> productPage) {
        this.productPage = productPage;
    }
}
