package com.intern.webstoreproducts.old.controller;

import com.intern.webstoreproducts.old.doman.response.OneProductInStoreResponse;
import com.intern.webstoreproducts.old.doman.response.PageStoreProductsResponse;
import com.intern.webstoreproducts.old.service.StoreProductsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/web02")
@CrossOrigin(origins = "*")
public class StoreProductsController {
    @Autowired
    StoreProductsService storeProductsService;

    //Tim thong tin tat ca product trong store
    @GetMapping(value = "/{idStore}/product")
    public ResponseEntity<PageStoreProductsResponse> findAllProductsInStore(@PathVariable Integer idStore, @RequestParam(value = "page", defaultValue = "0") Integer page,
                                                                            @RequestParam(value = "size", defaultValue = "10") Integer size) {
        return ResponseEntity.ok(storeProductsService.findAllProductsInStore(idStore, page, size));
    }

    //Tim thong tin chi tiet 1 product co trong store
    @GetMapping(value = "/{idStore}/product/{idProduct}")
    public ResponseEntity<OneProductInStoreResponse> findProductInStore(@PathVariable Integer idStore, @PathVariable Integer idProduct) {
        return ResponseEntity.ok(storeProductsService.findProductsInStore(idStore, idProduct));
    }

}
