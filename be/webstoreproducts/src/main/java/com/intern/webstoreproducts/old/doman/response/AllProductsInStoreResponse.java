package com.intern.webstoreproducts.old.doman.response;

import com.intern.webstoreproducts.old.Dto.ProductDto;
import com.intern.webstoreproducts.old.comon.BaseResponse;
import com.intern.webstoreproducts.old.model.Product;
import com.intern.webstoreproducts.old.model.Store;

import java.util.List;

public class AllProductsInStoreResponse extends BaseResponse {
    public Integer dtoStoreId;
    public String storeName;
    public List<ProductDto> dtoList;

    public Integer getDtoStoreId() {
        return dtoStoreId;
    }

    public void setDtoStoreId(Integer dtoStoreId) {
        dtoStoreId = dtoStoreId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        storeName = storeName;
    }

    public List<ProductDto> getDtoList() {
        return dtoList;
    }

    public void setDtoList(List<ProductDto> dtoList) {
        this.dtoList = dtoList;
    }

    public AllProductsInStoreResponse loadFromListEntity(Store store, List<Product> listProduct) {
        AllProductsInStoreResponse allProductsInStoreResponse = new AllProductsInStoreResponse();
        allProductsInStoreResponse.setDtoStoreId(store.getIdStore());
        allProductsInStoreResponse.setStoreName(store.getName());
        ProductDto productDto = new ProductDto();
        for (Product p :
                listProduct) {
            productDto.setIdProduct(p.getIdProduct());
            productDto.setName(p.getName());
            allProductsInStoreResponse.dtoList.add(productDto);
        }
        return allProductsInStoreResponse;
    }
}



