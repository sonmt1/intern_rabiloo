package com.intern.webstoreproducts.old.comon;


import java.util.List;

public interface BaseLoadEntityList<T1 extends List<BaseDto>, T2 extends List<BaseEntity>> {
    T2 loadFromEntity(T1 entity);
}
