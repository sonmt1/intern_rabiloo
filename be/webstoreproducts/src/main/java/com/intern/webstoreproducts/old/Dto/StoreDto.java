package com.intern.webstoreproducts.old.Dto;

import com.intern.webstoreproducts.old.comon.BaseDto;
import com.intern.webstoreproducts.old.comon.BaseLoadEntity;
import com.intern.webstoreproducts.old.model.Store;

public class StoreDto extends BaseDto implements BaseLoadEntity<Store, StoreDto> {
    private Integer idStore;
    private String name;


    @Override
    public StoreDto loadFromEntity(Store entity) {
        StoreDto storeDto = new StoreDto();
        storeDto.idStore = entity.getIdStore();
        storeDto.name = entity.getName();
        return storeDto;
    }

    public Integer getIdStore() {
        return idStore;
    }

    public void setIdStore(Integer idStore) {
        this.idStore = idStore;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
