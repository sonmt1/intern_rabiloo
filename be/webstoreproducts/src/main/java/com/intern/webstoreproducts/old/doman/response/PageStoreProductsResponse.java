package com.intern.webstoreproducts.old.doman.response;

import com.intern.webstoreproducts.old.comon.BaseResponse;
import com.intern.webstoreproducts.old.model.Product;
import com.intern.webstoreproducts.old.model.Store;
import org.springframework.data.domain.Page;

public class PageStoreProductsResponse extends BaseResponse {

    Store store;
    Page<Product> productPage;

    public PageStoreProductsResponse() {

    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public Page<Product> getProductPage() {
        return productPage;
    }

    public void setProductPage(Page<Product> productPage) {
        this.productPage = productPage;
    }
}
