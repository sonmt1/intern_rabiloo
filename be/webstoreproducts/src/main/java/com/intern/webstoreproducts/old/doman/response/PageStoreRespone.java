package com.intern.webstoreproducts.old.doman.response;

import com.intern.webstoreproducts.old.comon.BaseResponse;
import com.intern.webstoreproducts.old.model.Store;
import org.springframework.data.domain.Page;

public class PageStoreRespone extends BaseResponse {
    Page<Store> storePage;

    public Page<Store> getStorePage() {
        return storePage;
    }

    public void setStorePage(Page<Store> storePage) {
        this.storePage = storePage;
    }
}
