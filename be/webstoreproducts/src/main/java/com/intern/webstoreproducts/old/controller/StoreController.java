package com.intern.webstoreproducts.old.controller;

import com.intern.webstoreproducts.old.doman.response.ListStoreDtoResponse;
import com.intern.webstoreproducts.old.doman.response.PageStoreRespone;
import com.intern.webstoreproducts.old.doman.response.StoreResponse;
import com.intern.webstoreproducts.old.model.Store;
import com.intern.webstoreproducts.old.service.StoreService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/web01")
@CrossOrigin(origins = "*")
public class StoreController {

    Logger logger = LoggerFactory.getLogger(StoreController.class);

    @Autowired
    private StoreService storeService;


    //API tim ta ca du lieu store
//    @RequestMapping(value = "/list-store",method = RequestMethod.GET)
//    public ResponseEntity<ListStoreDtoResponse> listStore(){
//        return ResponseEntity.ok(storeService.findAll());
//    }

    //API tim store theo id store
    @GetMapping(value = "/detail/{idStore}")
    public ResponseEntity<StoreResponse> findByIdStore(@PathVariable Integer idStore) {
        return ResponseEntity.ok(storeService.findById(idStore));
    }

    //API xoa logic store theo Store
    @DeleteMapping(value = "/delete")
    public ResponseEntity<StoreResponse> deleteByIdStore(@RequestBody Store store) {
        return ResponseEntity.ok(storeService.deleteByIdStore(store));
    }

    //API luu/ cap nhat store dau vao store duoc nhap tu nguoi dung
    @PostMapping(value = "/save")
    public ResponseEntity<StoreResponse> saveStore(@RequestBody Store store) {
        return ResponseEntity.ok(storeService.saveStore(store));
    }

    @GetMapping(value = "/list-store")
    public ResponseEntity<PageStoreRespone> pageStore(
            @RequestParam(value = "page", defaultValue = "0") Integer page,
            @RequestParam(value = "size", defaultValue = "10") Integer size) {
        logger.info(page.toString() + size.toString());
        return ResponseEntity.ok(storeService.findAllWithPaging(page, size));
    }

    @GetMapping(value = "/list-store-search")
    public ResponseEntity<ListStoreDtoResponse> searchStoreByNameLike(@RequestParam(value = "name") String name) {
        return ResponseEntity.ok(storeService.searchStoreByNameLike(name));
    }


}
