package com.intern.webstoreproducts.old.model;

import com.intern.webstoreproducts.old.Dto.StoreDto;
import com.intern.webstoreproducts.old.comon.BaseEntity;
import com.intern.webstoreproducts.old.comon.BaseLoadDto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "store")
public class Store extends BaseEntity implements Serializable, BaseLoadDto<StoreDto, Store> {

    public static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idStore;

    @Column(name = "name")
    private String name;

    @Column(name = "is_deleted")
    private Boolean isDeleted;

    public Store() {
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIdStore() {
        return idStore;
    }

    public void setIdStore(Integer idStore) {
        this.idStore = idStore;
    }

    @Override
    public String toString() {
        return "Store{" +
                "id=" + idStore +
                ", name='" + name + '\'' +
                ", isDeleted=" + isDeleted +
                '}';
    }


    @Override
    public Store loadFromDto(StoreDto dto) {
        Store store = new Store();
        store.setIdStore(dto.getIdStore());
        store.setName(dto.getName());
        return store;
    }
}
