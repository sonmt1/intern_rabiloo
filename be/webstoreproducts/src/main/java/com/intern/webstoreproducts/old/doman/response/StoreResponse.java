package com.intern.webstoreproducts.old.doman.response;

import com.intern.webstoreproducts.old.comon.BaseResponse;
import com.intern.webstoreproducts.old.model.Store;

public class StoreResponse extends BaseResponse {
    public Store store;

    public StoreResponse() {
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }
}
