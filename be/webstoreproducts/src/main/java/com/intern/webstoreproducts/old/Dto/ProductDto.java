package com.intern.webstoreproducts.old.Dto;

import com.intern.webstoreproducts.old.comon.BaseDto;
import com.intern.webstoreproducts.old.comon.BaseLoadEntity;
import com.intern.webstoreproducts.old.model.Product;

public class ProductDto extends BaseDto implements BaseLoadEntity<Product, ProductDto> {
    Integer idProduct;
    String name;

    public Integer getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(Integer idProduct) {
        this.idProduct = idProduct;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public ProductDto loadFromEntity(Product entity) {
        ProductDto productDto = new ProductDto();
        productDto.setIdProduct(entity.getIdProduct());
        productDto.setName(entity.getName());
        return productDto;
    }
}
