package com.intern.webstoreproducts.old.repository;

import com.intern.webstoreproducts.old.model.Store;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StoreRepository extends JpaRepository<Store, Integer> {
    Store findByIdStoreAndName(Integer id,String name);

    Store findByIdStore(Integer id);

    @Query("select s from Store s")
    Page<Store> findAll(Pageable pageable);

    @Query("select s from Store s where s.name = ?1")
    List<Store> findByName(String name);


    @Query("select s from Store s where s.name like concat('%',?1,'%')")
    List<Store>  findByNameLike(String name);
}
