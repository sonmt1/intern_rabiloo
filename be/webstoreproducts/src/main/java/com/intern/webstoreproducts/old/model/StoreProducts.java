package com.intern.webstoreproducts.old.model;

import com.intern.webstoreproducts.old.Dto.StoreProductDto;
import com.intern.webstoreproducts.old.comon.BaseEntity;
import com.intern.webstoreproducts.old.comon.BaseLoadDto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "store_products")
public class StoreProducts extends BaseEntity implements Serializable, BaseLoadDto< StoreProductDto, StoreProducts> {

    public static final long serialVersionUID = 2L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idStoreProducts;

    @Column(name = "id_store")
    private Integer idStore;

    @Column(name = "id_product")
    private Integer idProduct;

    public StoreProducts() {
    }

    public Integer getIdStoreProducts() {
        return idStoreProducts;
    }

    public void setIdStoreProducts(Integer idStoreProducts) {
        this.idStoreProducts = idStoreProducts;
    }

    public Integer getIdStore() {
        return idStore;
    }

    public void setIdStore(Integer idStore) {
        this.idStore = idStore;
    }

    public Integer getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(Integer idProduct) {
        this.idProduct = idProduct;
    }

    @Override
    public String toString() {
        return "StoreProducts{" +
                "idStoreProducts=" + idStoreProducts +
                ", idStore=" + idStore +
                ", idProduct=" + idProduct +
                '}';
    }

    @Override
    public StoreProducts loadFromDto(StoreProductDto dto) {
//        StoreProducts storeProducts = new StoreProducts();
//        storeProducts.idStore = dto.getIdStore();
//        storeProducts.idProduct = dto.getIdProduct();
//        return storeProducts;
        return null;
    }
}
