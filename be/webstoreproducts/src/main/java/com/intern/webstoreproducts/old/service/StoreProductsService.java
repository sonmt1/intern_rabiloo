package com.intern.webstoreproducts.old.service;

import com.intern.webstoreproducts.old.doman.response.OneProductInStoreResponse;
import com.intern.webstoreproducts.old.doman.response.PageStoreProductsResponse;
import com.intern.webstoreproducts.old.model.Product;
import com.intern.webstoreproducts.old.model.Store;
import com.intern.webstoreproducts.old.model.StoreProducts;
import com.intern.webstoreproducts.old.repository.ProductRepository;
import com.intern.webstoreproducts.old.repository.StoreProductsRepository;
import com.intern.webstoreproducts.old.repository.StoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StoreProductsService {

    @Autowired
    StoreProductsRepository storeProductsRepository;

    @Autowired
    StoreRepository storeRepository;

    @Autowired
    ProductRepository productRepository;

    PageStoreProductsResponse pageStoreProductsResponse;


    //Service xu ly lay tat ca thong tin product co trong store
    public PageStoreProductsResponse findAllProductsInStore(Integer idStore, Integer page, Integer size) {
        pageStoreProductsResponse = new PageStoreProductsResponse();
        Page<Product> productPage;
        if (page == null && size == null) {
            productPage = productRepository.findAll(PageRequest.of(0, 10));
            if (productPage.getContent().isEmpty()) {
                pageStoreProductsResponse.setCode("001");
                pageStoreProductsResponse.setMessage("No store exists yet!");
                return pageStoreProductsResponse;
            }
        }
        // Tim store theo idStore chuyen vao
        Store store = storeRepository.findByIdStore(idStore);
        System.err.println("idStore:"+idStore);
        if (store == null || store.getIsDeleted() == true) {
            //khong ton tai store -> gui respose tra ve code va message
            pageStoreProductsResponse.setCode("011");
            pageStoreProductsResponse.setMessage("Can not find store or store had been deleted");
            return pageStoreProductsResponse;
        }
        // Tim List store va product co trong store trung voi dieu kien bang idStore
        Page<StoreProducts> storeProductsPage = storeProductsRepository.findByIdStore(idStore, PageRequest.of(page, size));
        if (storeProductsPage.isEmpty()) {
            //store khong co product nao
            pageStoreProductsResponse.setCode("012");
            pageStoreProductsResponse.setMessage("Store has no products");
            return pageStoreProductsResponse;
        }
        List<Product> productList = new ArrayList<>();
        //tim tat ca product trong database product
        List<Product> productListFromDB = productRepository.findAll();
        if (productListFromDB.isEmpty()) {
            //khong co product nao da duoc them vao database
            pageStoreProductsResponse.setCode("013");
            pageStoreProductsResponse.setMessage("No products exist yet");
            return pageStoreProductsResponse;
        }
        //bien the hien tim thay tat ca thong tin product co trong store da duoc luu trong database
        int numberProdctInStoreCanNotFound = 0;
        StringBuilder idProductNotFound = new StringBuilder();
        //lay thong tin product co trong store da tim -> kiem tra trung voi ten trong list product
        // -> luu thong tin product vao mang de gui response
        int productId = 0;
        boolean productHadFound = false;
        for (StoreProducts s : storeProductsPage) {
            productId = s.getIdProduct();
            for (Product p : productListFromDB) {
                if (productId == p.getIdProduct()) {
                    productList.add(p);
                    productHadFound = true;
                    break;
                }

            }
            //khong tim thay product trung voi idProduct trong listProduct tra ve
            if (!productHadFound) {
                idProductNotFound.append(productId + ",");
                numberProdctInStoreCanNotFound++;
                productHadFound = false;
            }

        }

        if (numberProdctInStoreCanNotFound == 0) {
            //khi tat ca product trong store duoc tim thay
            pageStoreProductsResponse.ok();
        } else {
            //khi co it nhat 1 product trong store khong tim thay trong list product luu trong database
            pageStoreProductsResponse.setCode("014");
            pageStoreProductsResponse.setMessage("Can not found product id: " + idProductNotFound);
            return pageStoreProductsResponse;
        }

        // pageable
        Pageable pageable = PageRequest.of(page, size);

        //luu thong tin va gui ve controller
        pageStoreProductsResponse.setStore(store);
        List<Integer> productIds = new ArrayList<>();
        productList.forEach(product -> {
            productIds.add(product.getIdProduct());
        });
        Page<Product> productDtoPage = productRepository.findByIdProduct(productIds, pageable);
        System.err.println(productDtoPage.getTotalElements());
        System.err.println(productDtoPage.getTotalPages());
        System.err.println(productDtoPage.getContent());
        pageStoreProductsResponse.setProductPage(productDtoPage);
        return pageStoreProductsResponse;
    }

    //Service xu ly lay thong tin detail 1 product co trong store
    public OneProductInStoreResponse findProductsInStore(int idStore, int idProduct) {
        OneProductInStoreResponse oneProductInStoreResponse = new OneProductInStoreResponse();

        Store store = storeRepository.findByIdStore(idStore);
        //kiem tra ton tai store
        if (store == null || store.getIsDeleted()) {
            oneProductInStoreResponse.setCode("002");
            oneProductInStoreResponse.setCode("Can not find store!");
            return oneProductInStoreResponse;
        }

        //Tim thong tin idStore va idProduct co trong bang database luu tru
        StoreProducts storeProducts = storeProductsRepository.findByIdStoreAndIdProduct(idStore, idProduct);
        //Kiem tra thong tin luu tru tra ve
        if (storeProducts == null) {
            oneProductInStoreResponse.setCode("015");
            oneProductInStoreResponse.setMessage("Can not find product in store");
            return oneProductInStoreResponse;
        }

        Product product = productRepository.findByIdProduct(idProduct);
        //kiem tra ton tai cua product
        if (product == null) {
            oneProductInStoreResponse.setCode("016");
            oneProductInStoreResponse.setMessage("Can not find product!");
            return oneProductInStoreResponse;
        }

        //tra ve thong tin yeu cau
        oneProductInStoreResponse.ok();
        oneProductInStoreResponse.setStore(store);
        oneProductInStoreResponse.setProducts(product);

        return oneProductInStoreResponse;
    }
}
