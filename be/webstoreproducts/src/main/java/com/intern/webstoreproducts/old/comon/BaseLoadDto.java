package com.intern.webstoreproducts.old.comon;

public interface BaseLoadDto<T extends BaseDto, E extends BaseEntity> {
    public E loadFromDto(T dto);
}
