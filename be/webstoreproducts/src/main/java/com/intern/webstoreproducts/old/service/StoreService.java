package com.intern.webstoreproducts.old.service;

import com.intern.webstoreproducts.old.Dto.StoreDto;
import com.intern.webstoreproducts.old.doman.response.ListStoreDtoResponse;
import com.intern.webstoreproducts.old.doman.response.PageStoreRespone;
import com.intern.webstoreproducts.old.doman.response.StoreResponse;
import com.intern.webstoreproducts.old.model.Store;
import com.intern.webstoreproducts.old.repository.StoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StoreService {

    @Autowired
    StoreRepository storeRepository;

    ListStoreDtoResponse listStoreDtoResponse;
    StoreResponse storeResponse;
    PageStoreRespone pageStoreRespone;


    public ListStoreDtoResponse findAll() {

        listStoreDtoResponse = new ListStoreDtoResponse();
        //lay danh sach tat ca store
        List<Store> stores = storeRepository.findAll();
        StoreDto storeDto = new StoreDto();
        if (stores != null) {
            List<StoreDto> dtoList = new ArrayList<>();
            for (Store s :
                    stores) {
                dtoList.add(new StoreDto().loadFromEntity(s));
            }
            listStoreDtoResponse.setDtoList(dtoList);
            listStoreDtoResponse.ok();
            return listStoreDtoResponse;
        } else {
            listStoreDtoResponse.setCode("001");
            listStoreDtoResponse.setMessage("No store exists yet!");
            return listStoreDtoResponse;
        }
    }

    public PageStoreRespone findAllWithPaging(Integer page, Integer size) {
        System.err.println(page+size);
        pageStoreRespone = new PageStoreRespone();
        Page<Store> storePage;
        if (page == null && size == null) {
            storePage = storeRepository.findAll(PageRequest.of(0, 10));
            if (storePage.getContent().isEmpty()) {
                pageStoreRespone.setCode("001");
                pageStoreRespone.setMessage("No store exists yet!");
                return pageStoreRespone;
            }
        } else {
            storePage = storeRepository.findAll(PageRequest.of(page, size));
        }

        pageStoreRespone.ok();
        pageStoreRespone.setStorePage(storePage);
        return pageStoreRespone;
    }

    public ListStoreDtoResponse findByNameStore(String name) {
        listStoreDtoResponse = new ListStoreDtoResponse();
        if (name == null) {
            listStoreDtoResponse.setCode("008");
            listStoreDtoResponse.setMessage("name not entered!");
            return listStoreDtoResponse;
        }


        return listStoreDtoResponse;
    }

    public StoreResponse findById(int idStore) {
        storeResponse = new StoreResponse();
        Store store = storeRepository.findByIdStore(idStore);
        if (store != null) {
            storeResponse.ok();
            storeResponse.setStore(store);
            return storeResponse;
        } else {
            return null;
        }
    }


    public StoreResponse deleteByIdStore(Store store1) {
        storeResponse = new StoreResponse();
        Store store = storeRepository.findByIdStoreAndName(store1.getIdStore(), store1.getName());
        if (store == null) {
            storeResponse.setCode("002");
            storeResponse.setMessage("Can not find store!");

        } else {
            if (!store.getIsDeleted()) {
                store.setIsDeleted(true);
                store = storeRepository.save(store);
                if (store != null) {
                    storeResponse.ok();
                } else {
                    storeResponse.setCode("001");
                    storeResponse.setMessage("Save fail!");
                }
            } else if (store.getIsDeleted()) {
                storeResponse.setCode("003");
                storeResponse.setMessage("Store has been deleted!");
            } else {
                storeResponse.setCode("004");
                storeResponse.setMessage("Data in database is wrong!");
            }
        }
        storeResponse.setStore(store);
        return storeResponse;
    }

    public StoreResponse saveStore(Store store) {

        storeResponse = new StoreResponse();
        if (store == null) {
            storeResponse.setCode("005");
            storeResponse.setMessage("Store input is empty!");
            storeResponse.setStore(store);
            return storeResponse;
        }

        if (store.getName() == "") {
            storeResponse.setCode("006");
            storeResponse.setMessage("Store name input is empty!");
            storeResponse.setStore(store);
            return storeResponse;
        }

        //tao store
        if (store.getIdStore() == null) {
            List<Store> storeListFindByName = storeRepository.findByName(store.getName());
            if (!storeListFindByName.isEmpty()) {
                storeResponse.setCode("009");
                storeResponse.setMessage("name already exists");
                return storeResponse;
            }
        }

        if (store.getIsDeleted() == null) {
            store.setIsDeleted(false);
        }


        storeResponse.ok();
        storeResponse.setStore(store);
        store = storeRepository.save(store);
        if (store != null) {
            storeResponse.ok();
        } else {
            storeResponse.setCode("007");
            storeResponse.setMessage("Save fail!");
        }

        return storeResponse;
    }

    public ListStoreDtoResponse searchStoreByNameLike(String name){
        listStoreDtoResponse = new ListStoreDtoResponse();
        //khi name store tim khong co
        //afdasdf
        if(name == null){
            listStoreDtoResponse.setCode("006");
            listStoreDtoResponse.setMessage("Store name input is empty!000000");
            return listStoreDtoResponse;
        }
        
        
        List<Store> storeList = storeRepository.findByNameLike(name);
        if(storeList.isEmpty()){
            listStoreDtoResponse.setCode("010");
            listStoreDtoResponse.setMessage("Can not find store with name entered!");
            return listStoreDtoResponse;
        }

        List<StoreDto>  storeDtos = new ArrayList<>();
        StoreDto storeDto = new StoreDto();
        for (Store s :
                storeList) {
            storeDtos.add(storeDto.loadFromEntity(s));

        }
        listStoreDtoResponse.ok();
        listStoreDtoResponse.setDtoList(storeDtos);

        return listStoreDtoResponse;
    }
}
