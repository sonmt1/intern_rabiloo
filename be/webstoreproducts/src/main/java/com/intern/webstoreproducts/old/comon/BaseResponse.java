package com.intern.webstoreproducts.old.comon;

public class BaseResponse {
    String code;

    String message;

    public BaseResponse() {
    }



    public BaseResponse(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void ok(){
        this.code = "200";
        this.message = "Succes!";
    }



}
