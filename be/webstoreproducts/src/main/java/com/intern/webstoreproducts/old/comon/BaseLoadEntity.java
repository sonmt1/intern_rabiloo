package com.intern.webstoreproducts.old.comon;

public interface BaseLoadEntity<T extends BaseEntity, E extends BaseDto>{
    E loadFromEntity(T entity);
}
