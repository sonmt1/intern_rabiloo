package com.intern.webstoreproducts.old.Dto;

import com.intern.webstoreproducts.old.comon.BaseDto;
import com.intern.webstoreproducts.old.comon.BaseLoadEntity;
import com.intern.webstoreproducts.old.model.Product;
import com.intern.webstoreproducts.old.model.Store;
import com.intern.webstoreproducts.old.model.StoreProducts;

import java.util.List;

public class StoreProductDto extends BaseDto implements BaseLoadEntity<StoreProducts, StoreProductDto> {

    StoreDto storeDto;
    List<ProductDto> productDtoList;

    public StoreProductDto(Store store, List<Product> productList) {
        this.storeDto.loadFromEntity(store);
        ProductDto productDto = new ProductDto();
        for (Product p : productList) {
            this.productDtoList.add(productDto.loadFromEntity(p));
        }

    }

    public List<ProductDto> getProductDtoList() {
        return productDtoList;
    }

    public void setProductDtoList(List<ProductDto> productDtoList) {
        this.productDtoList = productDtoList;
    }


    @Override
    public StoreProductDto loadFromEntity(StoreProducts entity) {
        return null;
    }
}
