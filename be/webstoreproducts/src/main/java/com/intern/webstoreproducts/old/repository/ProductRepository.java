package com.intern.webstoreproducts.old.repository;

import com.intern.webstoreproducts.old.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product,Integer> {

    Product findByIdProduct(int idProduct);

    @Query("select p from Product p " +
            "where p.idProduct in ?1 ")
    Page<Product> findByIdProduct(List<Integer> productIds, Pageable pageable);


}
