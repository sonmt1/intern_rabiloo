package com.intern.webstoreproducts.old.doman.response;

import com.intern.webstoreproducts.old.Dto.StoreDto;
import com.intern.webstoreproducts.old.comon.BaseResponse;

import java.util.List;

public class ListStoreDtoResponse extends BaseResponse {
    public List<StoreDto> dtoList;

    public ListStoreDtoResponse() {
    }

    public List<StoreDto> getDtoList() {
        return dtoList;
    }

    public void setDtoList(List<StoreDto> dtoList) {
        this.dtoList = dtoList;
    }
}
