package com.intern.webstoreproducts.old.model;

import com.intern.webstoreproducts.old.Dto.ProductDto;
import com.intern.webstoreproducts.old.comon.BaseEntity;
import com.intern.webstoreproducts.old.comon.BaseLoadDto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "product")
public class Product extends BaseEntity implements Serializable, BaseLoadDto<ProductDto, Product> {
    public static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idProduct;

    @Column(name = "name")
    private String name;

    public Product() {
    }

    public Integer getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(Integer idProduct) {
        this.idProduct = idProduct;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Product{" +
                "idProduct=" + idProduct +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public Product loadFromDto(ProductDto dto) {
        Product product = new Product();
        product.idProduct = dto.getIdProduct();
        product.name = dto.getName();
        return product;
    }
}
