package com.intern.webstoreproducts.old.doman.response;

import com.intern.webstoreproducts.old.comon.BaseResponse;
import com.intern.webstoreproducts.old.model.Product;
import com.intern.webstoreproducts.old.model.Store;

public class OneProductInStoreResponse extends BaseResponse {
    private Store store;
    private Product product;

    public OneProductInStoreResponse() {
    }

    public OneProductInStoreResponse(Store store, Product product) {
        this.store = store;
        this.product = product;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public Product getProducts() {
        return product;
    }

    public void setProducts(Product product) {
        this.product = product;
    }
}
