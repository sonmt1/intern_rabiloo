package com.intern.webstoreproducts.new_01.dto;

public class ServerResponseDto {

    public static final ServerResponseDto SUCCESS = new ServerResponseDto(ResponseCase.SUCCESS);

    private final ResponseCase status;
    private Object data;

    public ServerResponseDto(ResponseCase responseCase) {
        this.status = responseCase;
    }

    public ServerResponseDto(ResponseCase status, Object data) {
        this.status = status;
        this.data = data;
    }

    public static ServerResponseDto success(Object data){
        return new ServerResponseDto(ResponseCase.SUCCESS, data);
    }

    public static ServerResponseDto error(Object data){
        return new ServerResponseDto(ResponseCase.ERROR, data);
    }


}
