package com.intern.webstoreproducts.new_01.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Constant {
    public static final String SUCCESS_MSG = "SUCCESS";
    public static final String ERROR_MSG = "ERROR";
}
