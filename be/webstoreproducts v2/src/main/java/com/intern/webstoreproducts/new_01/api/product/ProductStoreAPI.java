package com.intern.webstoreproducts.new_01.api.product;

import com.intern.webstoreproducts.new_01.entity.Product;
import com.intern.webstoreproducts.new_01.service.product.ProductStoreService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/api/v2/web02/{storeId}/common-product")
@RequiredArgsConstructor
public class ProductStoreAPI {
    private final ProductStoreService productStoreService;

    @GetMapping("/list-common-product")
    public List<Product> listWeb(@PathVariable Integer storeId,
                                 @RequestParam(value = "page", defaultValue = "0") Integer page,
                                 @RequestParam(value = "size", defaultValue = "10") Integer size) {
        System.err.println(storeId);
        System.err.println(page);
        System.err.println(size);
        System.err.println(storeId);
        if (storeId == null) {
            return Collections.emptyList();
        }
        return productStoreService.getAllByStoreId(storeId, page, size);
    }
}
