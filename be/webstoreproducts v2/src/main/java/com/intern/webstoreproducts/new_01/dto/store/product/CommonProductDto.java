package com.intern.webstoreproducts.new_01.dto.store.product;



public interface CommonProductDto {

    Long getStoreId();

    String getName();

    String getType();

    Boolean getIsDeleted();
}
