package com.intern.webstoreproducts.new_01.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.intern.webstoreproducts.new_01.util.Constant;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ResponseCase {
    SUCCESS(1000, Constant.SUCCESS_MSG),
    ADD_SUCCESS(2000,Constant.SUCCESS_MSG),
    UPDATE_SUCCESS(2001, Constant.SUCCESS_MSG),
    DELETE_SUCCESS(2002, Constant.SUCCESS_MSG),
    DELETE_FAILED(2005, Constant.ERROR_MSG),
    ERROR(4, "Error"),
    DUPLICATE(1020, "Duplicate"),
    NOT_FOUND(1030, "Cannot found entity!"),
    NOT_FOUND_STORE(1031, "Not found store!"),
    NOT_FOUND_PRODUCT(1032,"Not found product!"),
    DUPLICATE_DATA(1043, "Duplicate data!"),
    BAD_REQUEST(400, "Invalid Parameters!");

    public final int code;
    public final String message;

    ResponseCase(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
