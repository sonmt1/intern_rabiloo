package com.intern.webstoreproducts.new_01.service.product;

import com.intern.webstoreproducts.new_01.entity.Product;
import com.intern.webstoreproducts.new_01.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@RequiredArgsConstructor
public class ProductStoreService {

    public static final String GROUP_KEY = "group";
    private static final String PRODUCT_KEY = "products";

    ProductRepository productRepository;

    public List<Product> getAllByStoreId(Integer storeId, Integer page, Integer size) {
//        return productRepository.findByStoreIdAndIsDeleted(storeId, false, PageRequest.of(page, size));Page<CommonProductDto>
        List<Product> products = productRepository.findAll();
        for (Product p :
                products) {
            System.err.println(p.toString());
        }
        return productRepository.findAll();
    }


}
