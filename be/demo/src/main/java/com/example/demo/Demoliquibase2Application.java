package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demoliquibase2Application {

    public static void main(String[] args) {
        SpringApplication.run(Demoliquibase2Application.class, args);
    }

}
