import {createRouter, createWebHistory} from 'vue-router';
import store from '@/layout/store/store-layout.vue';


const router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: '/',
            name: 'store',
            component: store,
            children: [{
                name: 'product',
                path: '/product',
                component: () => import('../page/product-page.vue')
            }]
        }
    ]
})

export default router;